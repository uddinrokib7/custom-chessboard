var chessboard = initialize();
var chessboardElements = prepareChessboardElements();

document.getElementById("submit-btn").addEventListener("click", createChessboard);

function initialize() {
    var chessboard = document.getElementById("chessboard");

    // Clears any remnants in chessboard conatiner before generating a new one
    chessboard.innerHTML = "";
    
    return chessboard;
}

function prepareChessboardElements() {
    // Saves chessboard skeleton in variable
    var rowStart    = "<div class=\"row\">",
        blockType1  = "<span class=\"block-type-1\"></span>",
        blockType2  = "<span class=\"block-type-2\"></span>",
        rowEnd      = "</div>";

    var elements = {
        rowStart   : rowStart,
        rowEnd     : rowEnd,
        blockType1 : blockType1,
        blockType2 : blockType2
    };

    return elements;
}

function getUserInputs() {
    /* Collects user inputs.
     * Assures that col and row values are integer */
    var col = parseInt(document.getElementById("col-unit").value),
        row = parseInt(document.getElementById("row-unit").value),
        rawColorValues = document.getElementById("colors").value,

        // separtes color values into array elements
        colors = rawColorValues.split("-");

    // User cannot input row and col less than 1 and greater than 14
    if (!(col > 0 && col < 15) || !(row > 0 && row < 15)) {
        return false;
    }

    var inputs = {
        col    : col,
        row    : row,
        colors : colors
    }

    return inputs;
}

function decodeColor(color) {
    // color variable contains initials for color
    // Genrates full color name for CSS from the initials
    switch (color) {
        case "b": return "black";
        
        case "br": return "brown";
    
        case "w": return "white";
    
        case "g": return "gold";

        case "c": return "chocolate";
        
        default: return false;
    }

    return false;
}

function setColor(color1, color2) {
    // If no color values are sent defaults will be "black" and "white"
    var color1 = decodeColor(color1) || "black";
    var color2 = decodeColor(color2) || "white";

    var blockType1 = document.querySelectorAll(".block-type-1");
    var blockType2 = document.querySelectorAll(".block-type-2");
   
    // Sets style for each type of blocks in chessboard 
    for (var i = 0; i < blockType1.length; i++) {
        blockType1[i].style.backgroundColor = color1;
    }
 
    for (var i = 0; i < blockType2.length; i++) {
        blockType2[i].style.backgroundColor = color2;
    }

    // Sets border-color and box-shadow around chessboard container
    chessboard.style.border = "3px solid teal";
    chessboard.style.boxShadow = "0px 0px 15px #333"; 
}

function createChessboardContents(container, inputsObj) {
    /* Chessboard contains two different types of block 
    * Same type of blocks can not sit side by side both horizontally and vertically */

    // Creates number of blocks in both row and columns according to user inputs
    for (var i = 0; i < inputsObj.row; i++) {

        // Creates opening <div> tag for a row
        container += chessboardElements.rowStart;
        
        for (var j = 0; j < inputsObj.col; j++) {

            /* Every other block in a row are of same type
            * Detects every other block in the row by using even-odd technique */
            if (i%2) {

                /* Every other block in a column are of same type
                * Detects every other block in the column using even-odd technique */
                switch (j%2) {
                    case 1:
                        container += chessboardElements.blockType1;
                        break;                
                    case 0:
                        container += chessboardElements.blockType2;
                        break;
                }
            } else {

                /* Every other block in a column are of same type
                * Detects every other block in the column using even-odd technique */
                switch (j%2) {
                    case 1:
                        container += chessboardElements.blockType2;
                        break;               
                    case 0:
                        container += chessboardElements.blockType1;
                        break;
                }
            }
        }
        
        // Creates closing </div> tag for a row
        container += chessboardElements.rowEnd;
    }

    return container;

}

function createChessboard() {
    var htmlContent = "";
    var userInputs = getUserInputs();

    if (!userInputs) {
        window.confirm("Please fill in the Row & Col box with numbers within 1 to 14");
        
        return false;
    }

    htmlContent = createChessboardContents(htmlContent, userInputs);
    chessboard.innerHTML = htmlContent;    
    setColor(userInputs.colors[0], userInputs.colors[1]);

    return true;
}